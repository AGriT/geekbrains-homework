﻿#include <iostream>

using namespace std;

const int ncnstFirst = 4, ncnstSecond = 6;

int nFirst = 0, nSecond = 0;

int main()
{
	do
	{
		cout << ">";
		cin >> nFirst >> nSecond;
	} while ((!nFirst) && (!nSecond));

	cout << (((nFirst + nSecond >= 10) && (nFirst + nSecond <= 20)) ? "True\n" : "False\n");

	// part 2
	uint32_t nIsSimple = 0;
	bool blIsSimple = true;

	do
	{
		cout << ">";
		cin >> nIsSimple;
	} while (!nIsSimple);

	if (nIsSimple == 1)
		cout << "True\n";

	for (int i = 2; i <= nIsSimple / 2; i++)
	{
		if (nIsSimple % i == 0)
		{
			blIsSimple = false;
			cout << "False\n";
			break;
		}
	}

	if (blIsSimple)
		cout << "True\n";
	
	// part 3

	if (((ncnstFirst == 10) && (ncnstSecond == 10)) || (ncnstFirst + ncnstSecond == 10))
		cout << "True\n";
	else
		cout << "False\n";

	// part 4

	uint32_t nInsertYear = 0;

	do
	{
		cout << ">";
		cin >> nInsertYear;
	} while (!nInsertYear);

	if (nInsertYear % 4 == 0)
	{
		if (nInsertYear % 100 == 0)
		{
			if (nInsertYear % 400 == 0)
				cout << "True\n";
			else
				cout << "False\n";
		}
		else
			cout << "True\n";
	}
	else
		cout << "False\n";
}